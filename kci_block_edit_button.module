<?php

/**
 * @file
 * Hooks for kci edit button module.
 */

declare(strict_types = 1);

use Drupal\Core\Url;

/**
 * Implements hook_preprocess_block().
 *
 * {@inheritdoc}
 */
function kci_block_edit_button_preprocess_block(&$variables) {
  if (isset($variables['content']['#block_content'])) {
    if ($variables['elements']['#base_plugin_id'] === 'block_content') {
      $userCurrent = \Drupal::currentUser()->getAccount();
      if ($userCurrent->isAnonymous()) {
        return;
      }
      $content = $variables['content']['#block_content'];
      $current_path = \Drupal::service('path.current')->getPath();
      $language = \Drupal::service('language_manager')->getCurrentLanguage();
      $current_url = Url::fromUserInput($current_path, [
        'language' => $language,
      ]);
      $current_path = $current_url->toString();
      $current_path = '/' . trim($current_path, '/');
      $path_alias = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);
      $url = Url::fromRoute('entity.block_content.edit_form', ['block_content' => $content->id()], [
        'query' => [
          'destination' => $path_alias,
        ],
        'language' => $language,
      ]);
      $url = Url::fromRoute('entity.block_content.edit_form', ['block_content' => $content->id()], [
        'query' => [
          'destination' => $path_alias,
        ],
      ]);
      if (!$url->access($userCurrent)) {
        return;
      }
      $variables['title_prefix']['block_edit_button'] = [
        '#prefix' => '<div class="dropbutton-wrapper dropbutton-single"><div class="dropbutton-widget">
        <ul class="dropbutton"><li class="edit-node dropbutton-action">',
        '#title' => t('Edit'),
        '#type' => 'link',
        '#url' => $url,
        '#suffix' => '</li></ul>
        </div></div>',
        '#cache' => ['max-age' => 0],
      ];
      if (!$userCurrent->hasPermission('administer blocks')) {
        unset($variables['title_suffix']['contextual_links']);
      }
    }
  }
}
