To install this module with composer follow the instructions below:

## Instructions

1.  Update composer.json file using following code under 'repositories' section:
```
"kci.kci_block_edit_button": {
            "type": "package",
            "package": {
                "name": "kci/kci-block-edit-button",
                "version": "1.2.2",
                "type": "drupal-module",
                "extra": {
                    "installer-name": "kci_block_edit_button"
                },
                "source": {
                    "url": "https://kci@bitbucket.org/kci/block-edit-button.git",
                    "type": "git",
                    "reference": "v1.2.2"
                },
                "require": {
                    "composer/installers": "~1.0"
                }
            }
        }
```
2.  Run 'composer require kci/kci-block-edit-button'
